from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session
from flask_login import LoginManager
from os.path import exists
from .router import router
from colorama import Fore, Style

db = SQLAlchemy()

def app_factory():
    app = Flask(__name__)
    app.config.from_pyfile("conf.py")

    db.init_app(app)
    session = Session(app)
    
    router(app)

    if not exists("core/data.db"):
        print("Creating the database")
        db.create_all(app=app)
        print(Fore.GREEN + "Created database!")
    else:
        print(Fore.BLUE + "Database already exists")
        
    print(Style.RESET_ALL)

    from .models import User
    login_manager = LoginManager()
    login_manager.login_view = "views.login"
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    from flask import current_app
    with app.app_context():
        if current_app.config["GENERATE_TESTIMONIALS"] == True:
            print(Fore.BLUE + "Generating the testimonial images")
            print(Style.RESET_ALL)
            
            from .com import Utils
            if Utils.CommonFunctions.generateTestimonials() == True:
                print(Fore.GREEN + "Testimonials images generated successfully!")
                print(Style.RESET_ALL)
    
    print(Fore.BLUE + "App Factory Initalized Successfully!")
    print(Fore.RED + """
    _   _           _             
    | | | |_ __   __| | ___   __ _ 
    | | | | '_ \ / _` |/ _ \ / _` |
    | |_| | |_) | (_| | (_) | (_| |
     \___/| .__/ \__,_|\___/ \__, |
         |_|                 |___/ 
    """)
    print(Style.RESET_ALL)
    return app

wsgi_app = app_factory()