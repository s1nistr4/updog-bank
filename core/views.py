from flask import Blueprint, render_template, redirect, request, url_for, flash, abort
from flask_login import login_required, login_user, logout_user, current_user
from .models import User, Transactions, Account
from .com import Utils
from .conf import FAKE_SLOW_PAGE, OVERLAY, QUICK_SIGN_OUT, SHOW_WELCOME, RANDOM_REFRESH, MAX_NUM_LENGTH 
from markdown2 import markdown_path
from random import choice
from core import db
from sqlalchemy import desc

views = Blueprint("views", __name__)

settings = {
    "fakeSlowPage": FAKE_SLOW_PAGE,
    "fakeOverlay": OVERLAY,
    "quickSignOut": QUICK_SIGN_OUT,
    "showWelcome": SHOW_WELCOME,
    "randomRefresh": RANDOM_REFRESH,
    "maxNumLength": MAX_NUM_LENGTH,
}

@views.route("/", methods=["GET", "POST"])
def index():
    slogan = choice(Utils.slogans)
    return render_template("index.html", slogan=slogan, settings=settings, user=current_user)

@views.route("/config/", methods=["GET", "POST"])
def config(): 
    if request.method == "POST":
        def addAccountAction(request):
            name = request.form["account_name"]
            balance = request.form["account_balance"]
            account_user_id = int(request.form["account_user_id"])
            validate = Utils.CommonFunctions.valAccountAction(name, balance)

            if "valid" in validate:
                account = Account(uid=Utils.CommonFunctions.genUid(), name=name, balance=balance, user_id=account_user_id)
                db.session.add(account)
                db.session.commit()
            
            elif "valid" not in validate and "flashed" in validate:
                flash(validate["flashed"], category="error")

            return redirect(url_for("views.config"))

        def addTransactionAction(request):
            name = request.form.get("transaction_name")
            amount = request.form.get("transaction_amount")
            transaction_type = request.form.get("transaction_type")
            transaction_account_id = request.form.get("transaction_account_id")
            validate = Utils.CommonFunctions.valTransactionAction(name, amount, transaction_type, transaction_account_id)

            if "valid" in validate:
                transaction = Transactions(uid=Utils.CommonFunctions.genUid(), name=name, amount=amount, transaction_type=transaction_type, account_id=transaction_account_id)
                db.session.add(transaction)
                db.session.commit()
            
            elif "valid" not in validate and "flashed" in validate:
                flash(validate["flashed"], category="error")

            return redirect(url_for("views.config"))

        def signUpAction(request):
            email = request.form["email"]
            name = request.form["name"]
            password1 = request.form["password1"]
            password2 = request.form["password2"]
            account_number = int(request.form["account_number"])
            routing_number = int(request.form["routing_number"])
            balance = request.form["balance"]
            validate = Utils.CommonFunctions.valSignup(email, name, password1, password2, account_number, routing_number)
            
            if "valid" in validate:
                user = User(uid=Utils.CommonFunctions.genUid(), email=email, name=name, password=password1, account_number=account_number, routing_number=routing_number, balance=balance)
                db.session.add(user)
                db.session.commit()

                login_user(user, remember=True)

                return redirect(url_for("views.index"))
            
            elif "valid" not in validate and "flashed" in validate:
                flash(validate['flashed'], category="error")
                return redirect(url_for("views.config"))

        if request.form.get("email", False) != False and request.form.get("name", False) != False and request.form.get("password1", False) != False and request.form.get("password2", False) != False and request.form.get("account_number", False) != False and request.form.get("routing_number", False) != False and request.form.get("balance", False):
            signUpAction(request)
        
        elif request.form.get("account_name", False) != False and request.form.get("account_balance", False) != False and request.form.get("account_user_id", False) != False:
            addAccountAction(request)
        
        elif request.form.get("transaction_name", False) != False and request.form.get("transaction_amount", False) != False and request.form.get("transaction_type", False) != False and request.form.get("transaction_account_id", False) != False:
            addTransactionAction(request)
        
        else:
            flash("No data", category="error")

        return redirect(url_for('views.config'))
    
    else:
        users = User.query.all()
        return render_template("config.html", settings=settings, user=current_user, users=users)

@views.route("/login/", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        email = request.form["email"]
        password = request.form["password"]
        
        validate = Utils.CommonFunctions.valLogin(email, password)

        if "valid" in validate:
            user = User.query.filter_by(email=email).first()
            login_user(user, remember=True)
            flash(validate["flashed"], category="success")

            return redirect(url_for('views.index'))
        
        elif "valid" not in validate and "flashed" in validate:
            flash(validate["flashed"], category="error")
            return redirect(url_for('views.login'))
    
    else:
        return render_template("login.html", settings=settings, user=current_user)

@views.route("/profile/")
@login_required
def profile():
    return redirect(url_for('views.select_account'))

@views.route("/profile/select_account/")
@login_required
def select_account():
    c_nums = Utils.CommonFunctions.genCensoredNumbers(current_user.account_number, current_user.routing_number)
    
    return render_template("profile/select_account.html", 
        settings=settings, 
        user=current_user, 
        balance="{:,}".format(float(current_user.balance)),
        account_number=c_nums["account_num"], 
        routing_number=c_nums["routing_num"],
        account_num_stars=c_nums["account_num_stars"],
        routing_num_stars=c_nums["routing_num_stars"]
    )

@views.route("/profile/account/<string:uid>/")
@login_required
def account(uid):
    account = Account.query.filter_by(uid=uid).first()

    if account == None:
        abort(404)

    account_transactions = Transactions.query.filter_by(account_id=account.id).order_by(Transactions.created.desc()).all()
    c_nums = Utils.CommonFunctions.genCensoredNumbers(current_user.account_number, current_user.routing_number)

    return render_template("profile/account.html", 
        settings=settings, 
        user=current_user, 
        balance="{:,}".format(float(current_user.balance)),
        account_number=c_nums["account_num"], 
        routing_number=c_nums["routing_num"],
        account_num_stars=c_nums["account_num_stars"],
        routing_num_stars=c_nums["routing_num_stars"],
        account=account,
        account_transactions=account_transactions
    )

@views.route("/signout")
@login_required
def signout():
    logout_user()
    flash("You have been logged out", category="success")
    return redirect(url_for('views.index'))

@views.route("/info/tos/")
def tos():
    md = markdown_path("core/md/tos.md")
    return render_template("info/tos.html", md=md, settings=settings, user=current_user)

@views.route("/info/privacy/")
def privacy():
    md = markdown_path("core/md/privacy.md")
    return render_template("info/privacy.html", md=md, settings=settings, user=current_user)

@views.app_errorhandler(403)
def error_403(e):
    return render_template("40x/403.html", settings=settings, user=current_user)

@views.app_errorhandler(404)
def error_404(e):
    return render_template("40x/404.html", settings=settings, user=current_user)
