"use strict";
const cookiesPopup = document.querySelector(".cookie-popup-frame");

// progremr 
try {
    var cookieRespYes = document.querySelector(".cookies-yes");
    var cookieRespNo = document.querySelectorAll(".cookies-no");
    var cookieRespRecieved = document.querySelector(".cookies-recieve");
    var cookies1 = document.querySelector(".cookies-1");
    var cookies2 = document.querySelector(".cookies-2");
    var cookies3 = document.querySelector(".cookies-3");
} catch (ReferenceError) {
    // pass
};

const zombo = document.querySelector(".zombo-com-frame");
const zomboEnterButton = document.querySelectorAll(".zombo-enter-button");
const zomboExitButton = document.querySelector(".zombo-exit-button");

$(document).ready(function () {
    clippy.load('Clippy', (agent) => {
        agent.show();
        let randomMessages;

        setInterval((randomMessages) => {
            randomMessages = [
                "Kittens who are taken along on short, trouble-free car trips to town tend to make good passengers when they get older. They get used to the sounds and motions of traveling and make less connection between the car and the visits to the vet.",
                "The ancient Egyptians were the first to tame the cat (in about 3000 BC), and used them to control pests.",
                "Siamese is very intelligent and able to learn to play fetch, walk on the leash and perform simple tricks. It likes to play with puzzle toys and other brain games",
                "Cats that live together sometimes rub each others heads to show that they have no intention of fighting. Young cats do this more often, especially when they are excited.",
                "A cat’s back is extremely flexible because it has up to 53 loosely fitting vertebrae. Humans only have 34.",
                "The claws on the cat’s back paws aren’t as sharp as the claws on the front paws because the claws in the back don’t retract and, consequently, become worn.",
                "A female cat can be referred to as a molly or a queen, and a male cat is often labeled as a tom.",
                "Isaac Newton invented the cat flap. Newton was experimenting in a pitch-black room. Spithead, one of his cats, kept opening the door and wrecking his experiment. The cat flap kept both Newton and Spithead happy.",
                "A cat has more bones than a human; humans have 206, but the cat has 230 (some cites list 245 bones, and state that bones may fuse together as the cat ages).",
                "Cat families usually play best in even numbers. Cats and kittens should be acquired in pairs whenever possible."
            ];

            let randomIndex = Math.floor(Math.random() * randomMessages.length);
            let message = randomMessages[randomIndex];

            agent.animate();
            agent.speak(message);
        }, 10000);
    });
});

function adBanner() {
    let advertisement;
    let adUrls;
    let currentAdUrl;

    function setAd(advertisement, adUrls, currentAdUrl) {
        advertisement = document.querySelector("#advertisement");
        adUrls = [
            "/static/img/ads/1.gif",
            "/static/img/ads/2.png",
            "/static/img/ads/3.gif",
            "/static/img/ads/4.png",
        ];

        currentAdUrl = Math.floor(Math.random() * adUrls.length);
        advertisement.setAttribute("src", adUrls[currentAdUrl]);
        console.log(`Set the image attribute to ${adUrls[currentAdUrl]}!!`);
    }

    setAd();
    setInterval(setAd, 12000);
};

function foxSay() {
    let statNum = Math.floor(Math.random() * 100); // :)

    let facts = [
        "90% of gambling addicts give up right when they're about to win big.",
        "By reading this text, you have officially lost the game.", 
        "The average person can't look up while sticking their tongue out at the same time.",
        `${statNum}% of statistics are made up on the spot`,
    ];

    let currentFact = Math.floor(Math.random() * facts.length);
    let fact = document.getElementById("fact");
    fact.innerText = facts[currentFact];
};

function showCookiePopup() {
    if (sessionStorage.getItem("show-cookie-popup") != "false") {
        setTimeout(() => {
            cookiesPopup.style.pointerEvents = "none";
            cookiesPopup.style.animation = "fadeIn 10s ease-in forwards";
            cookiesPopup.style.display = "block";

            setTimeout(() => {
                cookiesPopup.style.pointerEvents = "all";
            }, 10000)

        }, 3000);
    };
};

function cookieClickedYes() {
    cookies1.style.display = "none";
    cookies2.style.display = "block";
};

function cookieClickedRecievedCookies() {
    cookies2.style.display = "none";
    cookies3.style.display = "block";
    
};

function cookieClickedNo() {
    cookiesPopup.style.display = "none";
    sessionStorage.setItem("show-cookie-popup", "false");
};

function zomboShow() {
    zombo.style.display = "block";
}

function zomboHide() {
    zombo.style.display = "none";
}

adBanner();
foxSay();
showCookiePopup();

try {
    cookieRespYes.addEventListener("click", cookieClickedYes);
    cookieRespNo[0].addEventListener("click", cookieClickedNo);
    cookieRespNo[1].addEventListener("click", cookieClickedNo);
    cookieRespRecieved.addEventListener("click", cookieClickedRecievedCookies);
} catch (ReferenceError) {
    // pass
};

zomboEnterButton[0].addEventListener("click", zomboShow);
zomboEnterButton[1].addEventListener("click", zomboShow);
zomboExitButton.addEventListener("click", zomboHide);
