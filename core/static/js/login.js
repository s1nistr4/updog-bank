const captcha = document.querySelector(".captcha-area");
const signInForm = document.querySelector(".signinform");

signInForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const captchaResponseButton = document.querySelector(".submit-response-button");
    const captchaResponse = document.querySelector(".captcha-response");

    captcha.style.display = "block";

    captchaResponseButton.addEventListener("click", () => {
        if (captchaResponse.value < 10) {
            signInForm.submit();
        } else {
            let captchaInvalid = document.querySelector(".captcha-invalid");
            captchaInvalid.style.display = "block";
        };
    });
});